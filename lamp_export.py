from . import hydraPy as hy
from . import geometry_export
import numpy as np
from .utils import get_real_path


class LampTracker:

    # for partial mesh export
    KnownExportedObjects = set()
    KnownModifiedObjects = set()
    NewExportedObjects = set()

    def __init__(self, scene, filepath):
        self.curr_scene = scene
        self.filepath = filepath

        self.ExportedLamps = geometry_export.ExportCache('ExportedLamps')
        self.ExportedObjects = geometry_export.ExportCache('ExportedObjects')
        self.ExportedFiles = geometry_export.ExportCache('ExportedFiles')
        # start fresh
        LampTracker.NewExportedObjects = set()
        self.objects_used_as_duplis = set()

    def buildAndExportLamp(self, obj, seq=0.0):

        obj_cache_key = (self.curr_scene, obj)

        if self.ExportedObjects.have(obj_cache_key):
            print("Exported Objects have {}".format(obj))
            return self.ExportedObjects.get(obj_cache_key)

        lamp_definitions = self.exportLamp(obj, seq=seq)

        self.ExportedObjects.add(obj_cache_key, lamp_definitions)

        return lamp_definitions

    def exportLamp(self, obj, base_frame=None, seq=0.0):
        if base_frame is None:
            base_frame = self.curr_scene.frame_current

        try:
            lamp_definitions = []
            lamp = obj.data

            if lamp is None:
                raise geometry_export.UnexportableObjectException('Cannot export lamp obj')

            lamp_cache_key = (self.curr_scene, obj.data, seq)

            if self.ExportedLamps.have(lamp_cache_key):
                lamp_definitions.append(self.ExportedLamps.get(lamp_cache_key))
                return lamp_definitions

            lampRef = hy.hrLightCreate(obj.name)
            hy.hrLightOpen(lampRef, hy.HR_WRITE_DISCARD)

            lightNode = hy.hrLightParamNode(lampRef)

            lightNode.attribute("type").set_value(lamp.hydra.hydra_light_type)

            if lamp.hydra.hydra_light_type in {'point', 'directional'}:
                lightNode.attribute("shape").set_value('point')
            else:
                lightNode.attribute("shape").set_value(lamp.hydra.area_shape)

            lightNode.attribute("distribution").set_value(lamp.hydra.light_distribution)

            if lamp.hydra.hydra_light_type == 'directional':
                lightNode.append_child("size").append_attribute("inner_radius").set_value(1.0)  #TODO: add param
                lightNode.child("size").append_attribute("outer_radius").set_value(1.0)
            elif lamp.hydra.hydra_light_type == 'area':
                if lamp.hydra.area_shape == 'rect':
                    lightNode.append_child("size").append_attribute("half_length").set_value(lamp.hydra.half_x)
                    lightNode.child("size").append_attribute("half_width").set_value(lamp.hydra.half_y)
                elif lamp.hydra.area_shape == 'cylinder':
                    lightNode.append_child("size").append_attribute("radius").set_value(lamp.hydra.radius)
                    lightNode.child("size").append_attribute("height").set_value(lamp.hydra.height)
                    lightNode.child("size").append_attribute("angle").set_value(lamp.hydra.angle)
                elif lamp.hydra.area_shape in {'disk', 'sphere'}:
                    lightNode.append_child("size").append_attribute("radius").set_value(lamp.hydra.radius)

            if lamp.hydra.light_distribution == 'ies':
                distribution = lightNode.append_child("ies")

                distribution.append_attribute("data").set_value(get_real_path(lamp.hydra.ies_path))
                #
                distribution.append_attribute("matrix")
                samplerMatrix = np.array([1, 0, 0, 0,
                                          0, 1, 0, 0,
                                          0, 0, 1, 0,
                                          0, 0, 0, 1], dtype=np.float32)
                hy.WriteMatrix4x4(distribution, "matrix", samplerMatrix.flatten())

            intensityNode = lightNode.append_child("intensity")
            intensityNode.append_child("color").append_attribute("val").set_value(' '.join(map(str, lamp.hydra.color)))
            intensityNode.append_child("multiplier").append_attribute("val").set_value(lamp.hydra.multiplier)

            if lamp.hydra.light_distribution == 'spot':
                lightNode.append_child("falloff_angle").append_attribute("val").set_value(lamp.hydra.falloff_angle)
                lightNode.append_child("falloff_angle2").append_attribute("val").set_value(lamp.hydra.falloff_angle2)

            hy.hrLightClose(lampRef)

            lamp_definition = (lampRef, lamp)

            self.ExportedLamps.add(lamp_cache_key, lamp_definition)

            lamp_definitions.append((lampRef, lamp_definition))

            return lamp_definitions

        except geometry_export.UnexportableObjectException as err:
            print('Object export failed, skipping this object: %s' % err)
            return None
