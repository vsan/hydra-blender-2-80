from . import hydraPy as hy
from . import geometry_export
import numpy as np
from .utils import get_real_path


class MaterialExportException(Exception):
    #print("Material Export Exception")
    pass


class MaterialTracker:

    KnownExportedObjects = set()
    KnownModifiedObjects = set()
    NewExportedObjects = set()

    def __init__(self, scene, filepath):
        self.curr_scene = scene
        self.filepath = filepath

        self.ExportedMaterials = geometry_export.ExportCache('ExportedMaterials')
        # start fresh
        MaterialTracker.NewExportedObjects = set()

    def convertBlenderMaterial(self, mat):

        return hy.HRMaterialRef()

    def exportMaterial(self, obj, mat_indices, seq=0.0):
        try:
            material_definitions = []

            # if self.ExportedLamps.have(lamp_cache_key):
            #     lamp_definitions.append(self.ExportedLamps.get(lamp_cache_key))
            #     return lamp_definitions

            for m_id in mat_indices:
                ob_mat = obj.material_slots[m_id].material

                hydra_mat = False

                matRef = hy.HRMaterialRef()
                if hydra_mat:
                    pass
                else:
                    matRef = self.convertBlenderMaterial(ob_mat)

                mat_definition = matRef
                mat_cache_key = (self.curr_scene, ob_mat, seq)
                self.ExportedMaterials.add(mat_cache_key, mat_definition)
                material_definitions.append(matRef)

            return material_definitions

        except MaterialExportException as err:
            print('Material export failed, skipping this object: %s' % err)
            return None
