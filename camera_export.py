import math
import mathutils
from . import hydraPy as hy


def get_worldscale(scene, as_scalematrix=True):
    ws = 1

    scn_us = scene.unit_settings

    if scn_us.system in {'METRIC', 'IMPERIAL'}:
        ws = scn_us.scale_length

    if as_scalematrix:
        return mathutils.Matrix.Scale(ws, 4)

    else:
        return ws


def look_at(scene, matrix):
    '''
    pos, look_at and up vectors from matrix
    '''
    mat_rot = mathutils.Matrix.Rotation(math.radians(-90.0), 4, 'X')

    m_matrix = mathutils.Matrix()
    ws = mathutils.Matrix.Scale(scene.unit_settings.scale_length, 4) #get_worldscale(scene)
    m_matrix = mat_rot @ matrix @ ws

    ws_scalar = get_worldscale(scene, as_scalematrix=False)
    m_matrix[0][3] *= ws_scalar
    m_matrix[1][3] *= ws_scalar
    m_matrix[2][3] *= ws_scalar

    # transpose to extract columns
    m_matrix = m_matrix.transposed()

    pos = m_matrix[3]
    forwards = -m_matrix[2]
    target = (pos + forwards)
    up = m_matrix[1]

    return pos, target, up


def export_camera(scene, cam_instance):

    camera = cam_instance.obj
    cam_ref = hy.hrCameraCreate("my camera")

    cam_matrix = camera.matrix_world.copy()
    pos, lookat, up = look_at(scene, cam_matrix)

    cam_instance.motion = [(0.0, cam_matrix)]

    fov = math.degrees(camera.data.angle)  # 2.0 * math.degrees(math.atan(0.5 * camera.data.sensor_height / camera.data.lens)) #

    hy.hrCameraOpen(cam_ref, hy.HR_WRITE_DISCARD)
    cam_node = hy.hrCameraParamNode(cam_ref)
    cam_node.append_child("fov").text().set(str(fov))
    cam_node.append_child("nearClipPlane").text().set(str(camera.data.clip_start))
    cam_node.append_child("farClipPlane").text().set(str(camera.data.clip_end))
    cam_node.append_child("up").text().set(' '.join(map(str, up)))
    cam_node.append_child("position").text().set(' '.join(map(str, pos)))
    cam_node.append_child("look_at").text().set(' '.join(map(str, lookat)))
    cam_node.append_child("tiltShiftX").text().set(str(camera.data.shift_x))
    cam_node.append_child("tiltShiftY").text().set(str(camera.data.shift_y))
    hy.hrCameraClose(cam_ref)

    return cam_ref

