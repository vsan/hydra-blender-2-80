import os.path
import bpy


def get_real_path(path):
    return bpy.path.abspath(path)
