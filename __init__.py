
bl_info = {
    "name": "Hydra",
    "author": "vsan",
    "version": (0, 0, 2),
    "blender": (2, 80, 0),
    "location": "File > Import-Export",
    "description": "Export scenes to hydra render",
    "support": "COMMUNITY",
    "category": "Import-Export"}

if "bpy" in locals():
    import importlib
    if "hydra_export" in locals():
        importlib.reload(hydra_export)
    if "geometry_export" in locals():
        importlib.reload(geometry_export)


import bpy
from bpy.props import StringProperty, BoolProperty, FloatProperty, EnumProperty
from bpy_extras.io_utils import ExportHelper
from . import hydra_export
from . import ui
from . import hydra_properties
from . import nodes
import nodeitems_utils
from nodeitems_utils import NodeCategory, NodeItem


class ExportToHydra(bpy.types.Operator, ExportHelper):
    bl_idname = "export_mesh.hydra"
    bl_label = "Hydra export"
    bl_options = {"REGISTER"}

    filename_ext = "."

    use_scene_unit: BoolProperty(
        name="Scene Unit",
        description="Apply current scene's unit scale to exported data",
        default=False,
    )
    
    global_scale: FloatProperty(
            name="Scale",
            min=0.01, max=1000.0,
            default=1.0,
            )

    def execute(self, context):

        from mathutils import Matrix

        if not self.filepath:
            raise Exception("filepath not set")

        scene = context.scene

        global_scale = self.global_scale
        if scene.unit_settings.system != 'NONE' and self.use_scene_unit:
            global_scale *= scene.unit_settings.scale_length

        # global_matrix = Matrix.Scale(global_scale, 4)

        exporter = hydra_export.HydraExporter(self.filepath, context)
#       hydra_export.HydraExporter.export_objects_test(self.filepath, scene)
        exporter.export()

        return {"FINISHED"}

    def draw(self, context):
        pass


# class HydraSocketUniversal(bpy.types.NodeSocket):
#     bl_idname = 'HydraSocketUniversal'
#     bl_label = 'Hydra Socket'
#     value_unlimited: bpy.props.FloatProperty(default=0.0)
#     value_0_1: bpy.props.FloatProperty(min=0.0, max=1.0, default=0.0)
#
#     def draw(self, context, layout, node, text):
#         space = context.space_data
#         tree = space.edit_tree
#         links = tree.links
#         if self.is_linked:
#             value = []
#             for link in links:
#                 if link.from_node == node:
#                     inps = link.to_node.inputs
#                     for inp in inps:
#                         if inp.bl_idname == "HydraSocketFloat_0_1" and inp.is_linked:
#                             prop = "value_0_1"
#                             if prop not in value:
#                                 value.append(prop)
#                         if inp.bl_idname == "HydraSocketFloatUnlimited" and inp.is_linked:
#                             prop = "value_unlimited"
#                             if prop not in value:
#                                 value.append(prop)
#
#             if len(value) == 1:
#                 layout.prop(self, "%s" % value[0], text=text)
#             else:
#                 layout.prop(self, "percent", text="Percent")
#         else:
#             layout.prop(self, "percent", text=text)
#
#     def draw_color(self, context, node):
#         return 1, 0, 0, 1
#
#
# class HydraSocketFloat_0_1(bpy.types.NodeSocket):
#     bl_idname = 'HydraSocketFloat_0_1'
#     bl_label = 'Hydra Socket'
#     default_value: bpy.props.FloatProperty(description="Input node Value_0_1", min=0, max=1, default=0)
#
#     def draw(self, context, layout, node, text):
#         if self.is_linked:
#             layout.label(text=text)
#         else:
#             layout.prop(self, "default_value", text=text, slider=True)
#
#     def draw_color(self, context, node):
#         return 0.5, 0.7, 0.7, 1
#
#
# class HydraSocketFloatUnlimited(bpy.types.NodeSocket):
#     bl_idname = 'HydraSocketFloatUnlimited'
#     bl_label = 'Hydra Socket'
#     default_value: bpy.props.FloatProperty(default=0.0)
#
#     def draw(self, context, layout, node, text):
#         if self.is_linked:
#             layout.label(text=text)
#         else:
#             layout.prop(self, "default_value", text=text, slider=True)
#
#     def draw_color(self, context, node):
#         return 0.7, 0.7, 1, 1
#
#
# class HydraSocketColor(bpy.types.NodeSocket):
#     bl_idname = 'HydraSocketColor'
#     bl_label = 'Hydra Socket'
#
#     default_value: bpy.props.FloatVectorProperty(
#             precision=4, step=0.01, min=0, soft_max=1,
#             default=(0.0, 0.0, 0.0), options={'ANIMATABLE'}, subtype='COLOR')
#
#     def draw(self, context, layout, node, text):
#         if self.is_output or self.is_linked:
#             layout.label(text=text)
#         else:
#             layout.prop(self, "default_value", text=text)
#
#     def draw_color(self, context, node):
#         return 1, 1, 0, 1
#
#
# class HydraSocketBRDF(bpy.types.NodeSocket):
#     bl_idname = 'HydraSocketBRDF'
#     bl_label = 'Hydra Socket'
#     default_value: bpy.props.IntProperty()
#
#     def draw(self, context, layout, node, text):
#         layout.label(text=text)
#
#     def draw_color(self, context, node):
#         return 0, 1, 0, 1
#
#
# class HydraShaderNodeCategory(NodeCategory):
#     @classmethod
#     def poll(cls, context):
#         return context.space_data.tree_type == 'ShaderNodeTree'#'HydraNodeTree'
#
#
# node_categories = [
#
#     HydraShaderNodeCategory("HYDRASHADER", "Hydra Shader", items=[
#         NodeItem("HydraDiffuseNode"),
#         NodeItem("HydraOutputNode")
#         ]),
#
#     ]

#
# nodesockets = [HydraSocketBRDF, HydraSocketColor, HydraSocketFloat_0_1, HydraSocketFloatUnlimited,
#                HydraSocketUniversal]


def menu_func(self, context):
    self.layout.operator(ExportToHydra.bl_idname, text="Hydra render")


def register():
    hydra_properties.register()
    ui.register()
    # nodeitems_utils.register_node_categories("HYDRANODES", node_categories)
    # for cls in nodesockets:
    #     bpy.utils.register_class(cls)

    # bpy.utils.register_class(nodes.HydraOutputNode)
    # bpy.utils.register_class(nodes.HydraDiffuseNode)
    # bpy.utils.register_class(nodes.HydraNodeTree)

    bpy.utils.register_class(ExportToHydra)

    bpy.types.TOPBAR_MT_file_export.append(menu_func)


def unregister():
    hydra_properties.unregister()
    ui.unregister()

    # nodeitems_utils.unregister_node_categories("HYDRANODES")
    # for cls in nodesockets:
    #     bpy.utils.unregister_class(cls)
    #
    # bpy.utils.unregister_class(nodes.HydraOutputNode)
    # bpy.utils.unregister_class(nodes.HydraDiffuseNode)
    # bpy.utils.unregister_class(nodes.HydraNodeTree)

    bpy.utils.unregister_class(ExportToHydra)

    bpy.types.TOPBAR_MT_file_export.remove(menu_func)


if __name__ == "__main__":
    register()

