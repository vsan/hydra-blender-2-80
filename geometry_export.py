from . import hydraPy as hy

import array
import numpy as np
from collections import Counter


class UnexportableObjectException(Exception):
    #print("Unexportable Object exception")
    pass


def is_subd_last(obj):
    return obj.modifiers and \
        obj.modifiers[len(obj.modifiers) - 1].type == 'SUBSURF'


def is_subd_displace_last(obj):
    if len(obj.modifiers) < 2:
        return False

    return (obj.modifiers[len(obj.modifiers) - 2].type == 'SUBSURF' and
            obj.modifiers[len(obj.modifiers) - 1].type == 'DISPLACE')


def is_deforming(obj):
    deforming_modifiers = {'ARMATURE', 'CAST', 'CLOTH', 'CURVE', 'DISPLACE',
                           'HOOK', 'LATTICE', 'MESH_DEFORM', 'SHRINKWRAP',
                           'SIMPLE_DEFORM', 'SMOOTH', 'WAVE', 'SOFT_BODY',
                           'SURFACE', 'MESH_CACHE'}
    if obj.type in {'MESH', 'SURFACE', 'FONT'} and obj.modifiers:
        # special cases for auto subd/displace detection
        if len(obj.modifiers) == 1 and is_subd_last(obj):
            return False
        if len(obj.modifiers) == 2 and is_subd_displace_last(obj):
            return False

        for mod in obj.modifiers:
            if mod.type in deforming_modifiers:
                return True

    return False


class ExportCache:
    def __init__(self, name='Cache'):
        self.name = name
        self.cache_keys = set()
        self.cache_items = {}
        self.serial_counter = Counter()

    def clear(self):
        self.__init__(name=self.name)

    def serial(self, name):
        s = self.serial_counter[name]
        self.serial_counter[name] += 1

        return s

    def have(self, ck):
        return ck in self.cache_keys

    def add(self, ck, ci):
        self.cache_keys.add(ck)
        self.cache_items[ck] = ci

    def get(self, ck):
        if self.have(ck):
            return self.cache_items[ck]

        else:
            raise Exception('Item %s not found in %s!' % (ck, self.name))


class MeshDefinition:

    def __init__(self, mesh_name: str, mesh_ref: hy.HRMeshRef, materials_dict: dict):
        self.name = mesh_name
        self.mesh_ref = mesh_ref
        self.material_dict = materials_dict


class GeometryTracker:

    # for partial mesh export
    KnownExportedObjects = set()
    KnownModifiedObjects = set()
    NewExportedObjects = set()

    def __init__(self, context, filepath):
        self.context = context
        self.curr_scene = context.scene
        self.filepath = filepath

        self.ExportedMeshes = ExportCache('ExportedMeshes')
        self.ExportedObjects = ExportCache('ExportedObjects')
        self.ExportedFiles = ExportCache('ExportedFiles')
        # start fresh
        GeometryTracker.NewExportedObjects = set()

        self.objects_used_as_duplis = set()

    def buildMesh(self, obj, seq=0.0):

        obj_cache_key = (self.curr_scene, obj)

        if self.ExportedObjects.have(obj_cache_key):
            return self.ExportedObjects.get(obj_cache_key)

        mesh_definitions = self.writeMesh(obj, seq=seq)

        self.ExportedObjects.add(obj_cache_key, mesh_definitions)

        return mesh_definitions

    def buildAndExportMesh(self, obj, seq=0.0):

        obj_cache_key = (self.curr_scene, obj)

        if self.ExportedObjects.have(obj_cache_key):
            print("Exported Objects have {}".format(obj))
            return self.ExportedObjects.get(obj_cache_key)

        print("before export mesh, ", obj.name)
        mesh_definitions = self.exportMesh(obj, seq=seq)

        self.ExportedObjects.add(obj_cache_key, mesh_definitions)

        return mesh_definitions

    def exportMesh(self, obj, base_frame=None, seq=0.0):

        if base_frame is None:
            base_frame = self.curr_scene.frame_current

        try:
            # mesh = obj.to_mesh(self.context.depsgraph, apply_modifiers=True)
            # mesh = obj.to_mesh()
            mesh = obj.to_mesh()

            if mesh is None:
                raise UnexportableObjectException('Cannot create render/export mesh')

            mesh_cache_key = (self.curr_scene, obj.data, seq)

            if self.allowShapeInstancing(obj) and self.ExportedMeshes.have(mesh_cache_key):
                # print("{} is instance".format(mesh.name_full))
                return self.ExportedMeshes.get(mesh_cache_key)

            uv_layer = None
            for layer in mesh.uv_layers:  # take first uv layer since hydra currently doesn't support multiple layers
                if layer:
                    uv_layer = layer
                    break

            # if len(uv_textures) > 0:
            #     if uv_textures.active and uv_textures.active.data:
            #         uv_layer = uv_textures.active.data
            # else:
            #     uv_layer = None

            # Export data
            points = array.array('f', [])
            normals = array.array('f', [])
            uvs = array.array('f', [])

            ntris = 0
            face_vert_indices = array.array('I', [])
            mat_indices = array.array('I', [])

            unique_materials = dict()
            material_names = [m.name if m else None for m in mesh.materials]
            #
            # npVerts = np.empty(len(mesh.vertices) * 4)
            # npNormals = np.empty(len(mesh.vertices) * 4)
            # npUVs = np.empty(len(mesh.vertices) * 2)
            # npIndices = np.empty(len(mesh.tessfaces) * 3, dtype=np.int32)
            # npMatIndices = np.empty(len(mesh.tessfaces), dtype=np.int32)

            # Caches
            vert_vno_indices = {}  # mapping of vert index to exported vert index for verts with vert normals
            vert_use_vno = set()  # Set of vert indices that use vert normals

            vert_index = 0  # exported vert index

            mesh.calc_loop_triangles()

            for face in mesh.loop_triangles:
                fvi = []
                mat_indices.append(face.material_index)

                if material_names:
                    unique_materials[material_names[face.material_index]] = face.material_index
                #unique_materials.add(mesh.materials[face.material_index].name_full)
                for j, vertex in enumerate(face.vertices):
                    v = mesh.vertices[vertex]

                    if uv_layer:
                        uv_coord = uv_layer.data[j].uv  # (uv_layer[face.index].uv[j][0], 1.0 - uv_layer[face.index].uv[j][1])

                    if face.use_smooth:
                        if uv_layer:
                            vert_data = (v.co[:], v.normal[:], uv_coord[:])

                        else:
                            vert_data = (v.co[:], v.normal[:])

                        if vert_data not in vert_use_vno:
                            points.extend(vert_data[0])
                            points.append(1.0)

                            normals.extend(vert_data[1])
                            normals.append(1.0)

                            if uv_layer:
                                uvs.extend(vert_data[2])

                            vert_vno_indices[vert_data] = vert_index

                            fvi.append(vert_index)

                            vert_index += 1
                        else:
                            fvi.append(vert_vno_indices[vert_data])
                    else:
                        if uv_layer:
                            vert_data = (v.co[:], face.normal[:], uv_coord[:])  # uv_layer[face.index].uv[j][:]
                        else:
                            vert_data = (v.co[:], face.normal[:])

                        points.extend(vert_data[0])
                        points.append(1.0)

                        normals.extend(vert_data[1])
                        normals.append(1.0)

                        if uv_layer:
                            uvs.extend(vert_data[2])

                        fvi.append(vert_index)

                        vert_index += 1

                #triangulate quad faces
                face_vert_indices.extend(fvi[0:3])
                ntris += 3

                if len(fvi) == 4:
                    face_vert_indices.extend((fvi[0], fvi[2], fvi[3]))
                    ntris += 3

            del vert_vno_indices
            del vert_use_vno

            meshRef = hy.hrMeshCreate(obj.name)
            hy.hrMeshOpen(meshRef, hy.HR_TRIANGLE_IND3, hy.HR_WRITE_DISCARD)
            hy.hrMeshVertexAttribPointer4f(meshRef, "pos", points, 0)
            hy.hrMeshVertexAttribPointer4f(meshRef, "norm", normals, 0)
            if uv_layer:
                hy.hrMeshVertexAttribPointer2f(meshRef, "texcoord", uvs, 0)
            hy.hrMeshPrimitiveAttribPointer1iNumPy(meshRef, "mind", np.array(mat_indices, dtype=np.int32), 0)
            hy.hrMeshMaterialId(meshRef, 0)
            hy.hrMeshAppendTriangles3(meshRef, len(face_vert_indices), face_vert_indices)
            hy.hrMeshClose(meshRef)

            # mesh_definition = (meshRef, mesh, unique_materials)

            mesh_definition = MeshDefinition(mesh_name=mesh.name,
                                             mesh_ref=meshRef,
                                             materials_dict=unique_materials)

            if self.allowShapeInstancing(obj):
                self.ExportedMeshes.add(mesh_cache_key, mesh_definition)
                #print("mesh_definition key - val : {} - {}".format(mesh_cache_key, mesh_definition))

            #mesh_definitions.append((meshRef, mesh_definition))

            return mesh_definition

        except UnexportableObjectException as err:
            print('Object export failed, skipping this object: %s' % err)
            return None

    def writeMesh(self, obj, base_frame=None, seq=0.0):
        """
        Debug mesh output as plain text
        """

        if base_frame is None:
            base_frame = self.curr_scene.frame_current

        try:
            mesh_definitions = []
            # mesh = obj.to_mesh(self.curr_scene, True, 'RENDER')
            mesh = obj.to_mesh()

            if mesh is None:
                raise UnexportableObjectException('Cannot create render/export mesh')

            # uv_textures = mesh.tessface_uv_textures
            # if len(uv_textures) > 0:
            #     if uv_textures.active and uv_textures.active.data:
            #         uv_layer = uv_textures.active.data
            # else:
            #     uv_layer = None
            uv_layer = None

            # Export data
            ntris = 0
            co_no_uv_cache = []
            face_vert_indices = []  # mapping of face index to list of exported vert indices for that face

            # Caches
            vert_vno_indices = {}  # mapping of vert index to exported vert index for verts with vert normals
            vert_use_vno = set()  # Set of vert indices that use vert normals

            vert_index = 0  # exported vert index
            mesh.calc_loop_triangles()

            for face in mesh.loop_triangles:
                fvi = []
                mi = face.material_index

                for j, vertex in enumerate(face.vertices):
                    v = mesh.vertices[vertex]

                    if uv_layer:
                        uv_coord = (uv_layer[face.index].uv[j][0], 1.0 - uv_layer[face.index].uv[j][1])

                    if face.use_smooth:

                        if uv_layer:
                            vert_data = (v.co[:], v.normal[:], uv_coord)

                        else:
                            vert_data = (v.co[:], v.normal[:])

                        if vert_data not in vert_use_vno:
                            vert_use_vno.add(vert_data)

                            co_no_uv_cache.append(vert_data)

                            vert_vno_indices[vert_data] = vert_index
                            fvi.append(vert_index)

                            vert_index += 1

                        else:
                            fvi.append(vert_vno_indices[vert_data])

                    else:

                        if uv_layer:
                            vert_data = (v.co[:], face.normal[:], uv_layer[face.index].uv[j][:])

                        else:
                            vert_data = (v.co[:], face.normal[:])

                        co_no_uv_cache.append(vert_data)

                        fvi.append(vert_index)

                        vert_index += 1

                face_vert_indices.append(fvi[0:3])
                ntris += 3

                if len(fvi) == 4:
                    face_vert_indices.append((fvi[0], fvi[2], fvi[3]))
                    ntris += 3

            del vert_vno_indices
            del vert_use_vno

            with open(self.filepath, 'a') as test_file:

                test_file.write("{} \n".format(obj.name))
                # vert_index == the number of actual verts needed
                test_file.write('element vertex %d\n' % vert_index)
                test_file.write('element face %d\n' % int(ntris / 3))

                # dump cached co/no/uv
                if uv_layer:
                    for co, no, uv in co_no_uv_cache:
                        test_file.write("coord: ")
                        test_file.write("{} {} {}".format(co[0], co[1], co[2]))
                        test_file.write("  normal: ")
                        test_file.write("{} {} {}".format(no[0], no[1], no[2]))
                        test_file.write("  uv: ")
                        test_file.write("{} {}".format(uv[0], uv[1]))
                        test_file.write("\n")

                else:
                    for co, no in co_no_uv_cache:
                        test_file.write("coord: ")
                        test_file.write("{} {} {}".format(co[0], co[1], co[2]))
                        test_file.write("  normal: ")
                        test_file.write("{} {} {}".format(no[0], no[1], no[2]))
                        test_file.write("\n")

                # dump face vert indices
                test_file.write("indices:\n")
                for face in face_vert_indices:
                    test_file.write("{} {} {} ".format(face[0], face[1], face[2]))
                test_file.write("\n")

            # test_file.close()
            del co_no_uv_cache
            del face_vert_indices

            return mesh

        except UnexportableObjectException as err:
            print('Object export failed, skipping this object: %s' % err)
            return None

    def allowShapeInstancing(self, obj):
        # Only allow instancing if the mesh is not deformed
        if is_deforming(obj):
            return False

        # If the mesh is only used once, instancing is a waste of memory
        # However, duplis don't increase the users count, so we count those separately
        if (not ((obj.parent and obj.parent.is_duplicator) or obj in self.objects_used_as_duplis)) and obj.data.users == 1:
            return False

        # Only allow instancing for duplis and particles in non-hybrid mode, or
        # for normal objects if the object has certain modifiers applied against
        # the same shared base mesh.
        if hasattr(obj, 'modifiers') and len(obj.modifiers) > 0 and obj.data.users > 1:
            instance = False

            for mod in obj.modifiers:
                # Allow non-deforming modifiers
                instance |= mod.type in {'COLLISION', 'PARTICLE_INSTANCE', 'PARTICLE_SYSTEM', 'SMOKE'}

            return instance

        else:
            return True
