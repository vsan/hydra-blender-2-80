# -*- coding: utf-8 -*-
"""

"""
from . import geometry_export
from . import lamp_export
from . import material_export
from . import hydraPy as hy
from .camera_export import export_camera
import ntpath
import numpy as np
import mathutils
import math
import bpy


class InvalidGeometryException(Exception):
    # print("Invalid Geometry Exception ")
    pass


class UnexportableObjectException(Exception):
    # print("Unexportable Object exception")
    pass


class Instance:
    obj = None
    mesh = None
    light = None
    matrices = []

    def __init__(self, obj, mesh=None, light=None, matrix=None):
        self.obj = obj

        if mesh is not None:
            self.mesh = mesh
        else:
            self.mesh = None

        if light is not None:
            self.light = light
        else:
            self.light = None

        if matrix is not None:
            mat_rot = mathutils.Matrix.Rotation(math.radians(-90.0), 4, 'X')
            mat = mat_rot @ matrix
            self.matrices = [(0.0, mat)]
        else:
            self.matrices = []

    def append_matrix(self, matrix, seq, is_deform=False):
        seqs = len(self.matrices)

        mat_rot = mathutils.Matrix.Rotation(math.radians(-90.0), 4, 'X')
        mat = mat_rot @ matrix

        if self.matrices:
            last_matrix = self.matrices[-1][1]
        else:
            last_matrix = None

        if not is_deform and (seqs > 1 and mat == last_matrix and
                              last_matrix == self.matrices[-2][1]):
            self.matrices[-1] = (seq, mat)

        else:
            self.matrices.append((seq, mat))


def is_object_visible(context, obj, is_dupli=False):

    return (obj in context.window.view_layer.objects or is_dupli) and not obj.hide_render


def is_light(obj):
    return obj.type == 'LIGHT'


def is_mesh(obj):
    return obj.type in {'MESH', 'CURVE', 'SURFACE', 'FONT'}


def object_render_hide_original(ob_type, dupli_type):
    # metaball exception, they duplicate self
    if ob_type == 'META':
        return False

    return dupli_type in {'VERTS', 'FACES', 'FRAMES'}


def object_render_hide_duplis(b_ob):
    parent = b_ob.parent

    return parent is not None and object_render_hide_original(b_ob.type, parent.dupli_type)


def object_render_hide(b_ob, top_level, parent_hide):
    # check if we should render or hide particle emitter
    hair_present = False
    show_emitter = False
    hide_emitter = False
    hide_as_dupli_parent = False
    hide_as_dupli_child_original = False

    for b_psys in b_ob.particle_systems:
        if b_psys.settings.render_type == 'PATH' and b_psys.settings.type == 'HAIR':
            hair_present = True

        if b_psys.settings.use_render_emitter:
            show_emitter = True
        else:
            hide_emitter = True

    if show_emitter:
        hide_emitter = False

    # hide original object for duplis
    parent = b_ob.parent
    while parent is not None:
        if object_render_hide_original(b_ob.type, parent.dupli_type):
            if parent_hide:
                hide_as_dupli_child_original = True
                break

        parent = parent.parent

    hide_mesh = hide_emitter

    if show_emitter:
        return False, hide_mesh

    elif hair_present:
        return hide_as_dupli_child_original, hide_mesh

    else:
        return (hide_as_dupli_parent or hide_as_dupli_child_original), hide_mesh


def get_obj_unique_id(b_ob, duplicator):
    if duplicator is not None:
        obj = b_ob.object
        persistent_id = ['%X' % i for i in b_ob.persistent_id if i < 0x7fffffff]
        unique_id = '%s_%s_Dupli(%s)' % (duplicator.name, obj.name, 'x'.join(persistent_id))

    else:
        obj = b_ob
        unique_id = b_ob.name

    return obj, unique_id


class HydraExporter:

    shape_instances = {}
    debug = True
    geoTracker = None
    lampTracker = None
    materialTracker = None
    scene_camera = None

    def __init__(self, filepath, context):
        # hy.hrInit("-copy_textures_to_local_folder 1 -local_data_path 1 ")
        self.filepath = filepath
        self.directory = ntpath.dirname(filepath)
        self.context = context
        self.scene = context.scene

    def export_objects_test(self):
        out = open(self.filepath, "w")

        for obj in self.scene.objects:
            out.write(obj.type + ": " + obj.name + "\n")
        out.close()

    def sync_light(self, instances, b_ob, duplicator=None, matr=None, base_frame=0, seq=0.0):
        (obj, unique_id) = get_obj_unique_id(b_ob, duplicator)

        if unique_id in instances:
            instances[unique_id].append_matrix(matr, seq)
        else:
            instances[unique_id] = Instance(obj, light=self.lampTracker.buildAndExportLamp(obj, seq=seq), matrix=matr)

    def export_object(self, instances, b_ob, duplicator=None, matr=None, hide_mesh=False, base_frame=0, seq=0.0):
        (obj, unique_id) = get_obj_unique_id(b_ob, duplicator)

        if self.debug:
            print("Object unique_id: {}".format(unique_id))

        if is_light(obj):
            self.sync_light(instances, b_ob, duplicator, matr, base_frame, seq)
            return

        if is_mesh(obj) and not hide_mesh:
            if duplicator is not None:
                self.geoTracker.objects_used_as_duplis.add(obj)

            if unique_id in instances:
                instance = instances[unique_id]
                #is_deform = is_deforming(obj)
                is_deform = False

                instance.append_matrix(matr, seq, is_deform)
                if is_deform:
                    instance.mesh = self.geoTracker.exportMesh(obj, base_frame=base_frame, seq=seq)
            else:
                instances[unique_id] = Instance(obj, mesh=self.geoTracker.buildAndExportMesh(obj, seq=seq), matrix=matr)

    def export_render_settings(self):
        render_ref = hy.hrRenderCreate("HydraModern")
        hy.hrRenderEnableDevice(render_ref, 0, True)

        hy.hrRenderOpen(render_ref, hy.HR_WRITE_DISCARD)
        node = hy.hrRenderParamNode(render_ref)
        node.force_child("width").text().set(self.scene.render.resolution_x *
                                             self.scene.render.resolution_percentage * 0.01)
        node.force_child("height").text().set(self.scene.render.resolution_y *
                                              self.scene.render.resolution_percentage * 0.01)
        node.force_child("method_primary").text().set("pathtracing")
        node.force_child("method_caustic").text().set("none")
        node.force_child("trace_depth").text().set(5)
        node.force_child("diff_trace_depth").text().set(3)
        node.force_child("maxRaysPerPixel").text().set(1024)
        node.force_child("outputRedirect").text().set(1)
        hy.hrRenderLogDir(render_ref, "/tmp/hydra_log", True)
        hy.hrRenderClose(render_ref)

        return render_ref

    def traverse_depsgraph(self):
        origframe = self.scene.frame_current
        depsgraph = self.context.evaluated_depsgraph_get()

        instances = {}
        for dup in depsgraph.object_instances:
            # if self.debug:
            #     print('Analyzing object %s : %s \n' % (dup, dup.object.type))

            if dup.is_instance:  # Real dupli instance
                if self.debug:
                    print("Dupli object %s \n" % dup.object.name)
                obj = dup.instance_object.original
                parent = dup.parent.original
                mat = dup.matrix_world.copy()
                (hide_obj, hide_mesh) = object_render_hide(obj, False, dup.type == 'GROUP')
                if not (dup.hide or obj.hide_render or hide_obj):
                    self.export_object(instances, obj, dup, mat, hide_mesh, origframe)
            else:  # Usual object
                obj = dup.object.original
                (hide_obj, hide_mesh) = object_render_hide(obj, True, True)
                if not hide_obj and not obj.hide_render:
                    if self.debug:
                        print("Synchronizing object %s \n" % obj.name)
                    matr = obj.matrix_world.copy()
                    self.export_object(instances, obj, None, matr, hide_mesh, origframe)
                else:
                    if self.debug:
                        print("object hidden: %s \n" % obj.name)

        return instances

    def export_materials(self):
        material_map = {}
        for material in bpy.data.materials:
            if material.users > 0:
                # mat_name = bpy.path.clean_name(material.name)
                mat_name = material.name
                if material.node_tree:
                    ntree = material.node_tree
                    if len(ntree.nodes) == 0:
                        print("empty node tree material: %s \n" % mat_name)
                    else:
                        out_node = next(
                            (n for n in ntree.nodes if n.bl_idname in ['ShaderNodeOutputMaterial', 'HydraOutputNode']),
                            None)
                        if out_node is None:
                            return

                        nodes_to_export = self.gather_nodes(out_node)
                        print("Shader node tree for material %s: \n" % mat_name)
                        for node in nodes_to_export:
                            print("node: %s" % node.name)

                        mat0 = hy.hrMaterialCreate(mat_name)
                        hy.hrMaterialOpen(mat0, hy.HR_WRITE_DISCARD)
                        matNode = hy.hrMaterialParamNode(mat0)
                        diff = matNode.append_child("diffuse")
                        diff.append_attribute("brdf_type").set_value("lambert")
                        diff.append_child("color").text().set("0.5 0.5 0.5")
                        hy.hrMaterialClose(mat0)

                        material_map[mat_name] = mat0.id

                    for node in ntree.nodes:
                        if node:
                            if node.bl_idname == "HydraOutputNode":
                                for inp in node.inputs:
                                    print("Hydra Output node: %s \n" % inp)
                                # if node.inputs["HydraSocketBRDF"].is_linked:
                                #     for link in ntree.links:
                                #         print("link from node: %s \n" % link.from_node.name)
                                #         if link.to_node.bl_idname == "HydraOutputNode":
                                #             pass
                                else:
                                    pass
                else:
                    print("material with no nodes: %s \n" % mat_name)
                    # export_material()
        return material_map

    def instance_hydra_objects(self, scn_ref, instances: dict, material_map: dict):

        hy.hrSceneOpen(scn_ref, hy.HR_WRITE_DISCARD)
        for id, instance in instances.items():
            # print("Instance.mesh: ", instance.mesh)
            matr = instance.matrices[0][1]

            if instance.mesh:
                if self.debug:
                    print("Instancing object: {}".format(instance.mesh.name))

                matrix = np.array(matr, dtype=np.float32)
                remap_list = []
                for mat_name, mat_id in instance.mesh.material_dict.items():
                    hydra_mat_id = material_map.get(mat_name, None)
                    if hydra_mat_id is not None:
                        remap_list.append(mat_id)
                        remap_list.append(hydra_mat_id)

                hy.hrMeshInstanceRemap(scn_ref, instance.mesh.mesh_ref, matrix.flatten(),
                                       np.array(remap_list, dtype=np.int32), len(remap_list))
            elif instance.light:
                if self.debug:
                    print("Instancing light object: {}".format(instance.light[0][1]))

                mat_rot = mathutils.Matrix.Rotation(math.radians(-90.0), 4, 'X')
                mat_rot2 = mathutils.Matrix.Rotation(math.radians(180.0), 4, 'Y')
                matr = matr @ mat_rot2 @ mat_rot
                matrix = np.array(matr, dtype=np.float32)
                hy.hrLightInstance(scn_ref, instance.light[0][0], matrix.flatten())
        hy.hrSceneClose(scn_ref)

    def gather_nodes(self, node):
        nodes = []
        for socket in node.inputs:
            if socket.is_linked:
                link = socket.links[0]
                for sub_node in self.gather_nodes(socket.links[0].from_node):
                    if sub_node not in nodes:
                        nodes.append(sub_node)

        # if hasattr(node, 'hydra_node_type') and node.hydra_node_type != 'output':
        #     nodes.append(node)
        # elif not hasattr(node, 'hydra_node_type') and node.bl_idname not in ['ShaderNodeOutputMaterial',
        #                                                                          'NodeGroupInput', 'NodeGroupOutput']:
        if node.bl_idname not in ['NodeGroupInput', 'NodeGroupOutput']:
            nodes.append(node)

        return nodes

    def export(self):
        try:
            if self.scene is None:
                raise Exception('Scene is not valid for export')

            print("scenelib directory: " + self.directory)
            info = hy.HRInitInfo()
            hy.hrSceneLibraryOpen(self.directory, hy.HR_WRITE_DISCARD, info)

            self.scene_camera = Instance(self.scene.camera)
            cam_ref = export_camera(self.scene, self.scene_camera)

            self.geoTracker = geometry_export.GeometryTracker(self.context, self.filepath)
            self.lampTracker = lamp_export.LampTracker(self.scene, self.filepath)
            # self.materialTracker = material_export.MaterialTracker(self.scene, self.filepath)

            instances = self.traverse_depsgraph()

            # material_map = self.export_materials()

            scn_ref = hy.hrSceneCreate("my scene")
            self.instance_hydra_objects(scn_ref=scn_ref, instances=instances, material_map=material_map)

            render_ref = self.export_render_settings()

            hy.hrFlush(scn_ref, hy.HRRenderRef(), cam_ref)

        except Exception as err:
            import traceback
            traceback.print_exc()

            return {'CANCELLED'}
