import bpy
import math
from bpy.props import (BoolProperty,
                       EnumProperty,
                       FloatProperty,
                       IntProperty,
                       PointerProperty,
                       StringProperty,
                       FloatVectorProperty,
                       CollectionProperty)
from bpy.types import PropertyGroup
from bpy.props import StringProperty


class HydraLightSettings(PropertyGroup):

    # update functions to indicate settings in viewport
    def update_light_type(self, context):
        lamp = self.id_data
        light_type = self.hydra_light_type

        if light_type == 'directional':
            lamp.type = 'SUN'
        elif light_type in {'area', 'portal'}:
            lamp.type = 'AREA'
        elif light_type == 'point':
            if self.light_distribution == 'spot':
                lamp.type = 'SPOT'
            else:
                lamp.type = 'POINT'

    def update_distribution(self, context):
        lamp = self.id_data
        light_distribution = self.light_distribution

        if self.hydra_light_type == 'point':
            if light_distribution == 'spot':
                lamp.type = 'SPOT'
            elif self.hydra_light_type == 'point':
                lamp.type = 'POINT'

    def update_area_shape(self, context):
        lamp = self.id_data
        area_shape = self.area_shape

        if area_shape in {'disk', 'sphere'}:
            lamp.shape = 'SQUARE'
            lamp.size = self.radius * 2
        elif area_shape == 'rect':
            lamp.shape = 'RECTANGLE'
            lamp.size = self.half_x * 2
            lamp.size_y = self.half_y * 2
        elif area_shape == 'cylinder':
            lamp.shape = 'RECTANGLE'
            lamp.size = self.radius * 2
            lamp.size_y = self.height * 2

    def update_radius(self, context):
        lamp = self.id_data
        lamp.size = self.radius * 2

    def update_size_x(self, context):
        lamp = self.id_data
        lamp.size = self.half_x * 2

    def update_size_y(self, context):
        lamp = self.id_data
        lamp.size_y = self.half_y * 2

    def update_spot_size(self, context):
        lamp = self.id_data
        lamp.spot_size = math.radians(self.falloff_angle2)

    def update_cylinder_size(self, context):
        lamp = self.id_data
        lamp.size_y = self.height

    hydra_light_type: EnumProperty(
        name="Light Type",
        update=update_light_type,
        items=[('area', 'area', 'Area Light'),
               ('directional', 'directional', 'Directional Light'),
               ('point', 'point', 'Point Light'),
               #('portal', 'Portal', 'Portal Light'),
               ],
        default='area'
    )

    area_shape: EnumProperty(
        name="Area Shape",
        update=update_area_shape,
        items=[('rect', 'rect', 'Rectangle'),
               ('disk', 'disk', 'Disk'),
               ('sphere', 'sphere', 'Sphere'),
               ('cylinder', 'cylinder', 'Cylinder'),],
        default='rect'
    )

    light_distribution: EnumProperty(
        name="Light Distribution",
        update=update_distribution,
        items=[('uniform', 'uniform', 'Uniform'),
               ('spot', 'spot', 'Spot'),
               ('ies', 'IES', 'IES'),
               ],
        default='uniform'
    )

    ies_path: StringProperty(
        name="IES",
        description="IES file path",
        subtype='FILE_PATH',
    )

    half_x: FloatProperty(
        name="Half Length",
        update=update_size_x,
        description="Rectangle light half length",
        min=0.000001,
        default=1.0,
        )

    half_y: FloatProperty(
        name="Half Width",
        update=update_size_y,
        description="Rectangle light half width",
        min=0.000001,
        default=1.0,
    )

    radius: FloatProperty(
        name="Radius",
        update=update_radius,
        description="Light radius",
        min=0.000001,
        default=1.0,
    )

    height: FloatProperty(
        name="Height",
        update=update_cylinder_size,
        description="Cylinder light height",
        min=0.000001,
        default=1.0,
    )

    angle: FloatProperty(
        name="Angle",
        description="Cylinder light angle",
        min=0.1, max=360.0,
        default=360.0,
    )

    falloff_angle: FloatProperty(
        name="Falloff angle outer",
        description="Spot distribution outer falloff angle",
        min=0.1, max=180.0,
        default=60.0,
    )

    falloff_angle2: FloatProperty(
        name="Falloff angle inner",
        update=update_spot_size,
        description="Spot distribution inner falloff angle",
        min=0.1, max=180.0,
        default=45.0,
    )

    inner_radius: FloatProperty(
        name="Inner radius",
        description="Directional light inner radius",
        min=0.0001,
        default=10.0,
    )

    outer_radius: FloatProperty(
        name="Outer radius",
        description="Directional light outer radius",
        min=0.0001,
        default=50.0,
    )

    multiplier: FloatProperty(
        name="Multiplier",
        description="Intensity multiplier",
        min=0.0,
        default=10.0,
    )

    color: FloatVectorProperty(
        name="Color",
        description="Light color",
        subtype='COLOR',
        min=0.0,
        max=1.0,
    )


def register():
    bpy.utils.register_class(HydraLightSettings)
    bpy.types.Light.hydra = PointerProperty(
            name="Hydra Lamp Settings",
            description="Hydra lamp settings",
            type=HydraLightSettings,
            )


def unregister():
    bpy.utils.unregister_class(HydraLightSettings)
    del bpy.types.Light.hydra
