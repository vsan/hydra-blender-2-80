import bpy

from bpy.types import (
        Panel,
        Menu,
        Operator,
        )


class HydraButtonsPanel:
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "render"
    COMPAT_ENGINES = {'HYDRA', 'CYCLES'}

    @classmethod
    def poll(cls, context):
        rd = context.scene.render
        return rd.engine in cls.COMPAT_ENGINES


class HydraLamp_PT_lamp(HydraButtonsPanel, Panel):
    bl_label = "Hydra render light settings"
    bl_context = "data"

    @classmethod
    def poll(cls, context):
        return context.light and HydraButtonsPanel.poll(context)

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True

        lamp = context.light
        hlamp = lamp.hydra

        layout.prop(hlamp, "hydra_light_type", expand=True)

        if hlamp.hydra_light_type == 'area':
            layout.prop(hlamp, "area_shape", text="Shape", expand=True)
            if hlamp.area_shape == 'rect':
                layout.prop(hlamp, "half_x", text="Half size X")
                layout.prop(hlamp, "half_y", text="Half size Y")
            elif hlamp.area_shape in {'disk', 'sphere'}:
                layout.prop(hlamp, "radius", text="Radius")
            elif hlamp.area_shape == 'cylinder':
                layout.prop(hlamp, "radius", text="Radius")
                layout.prop(hlamp, "height", text="Height")
                layout.prop(hlamp, "angle", text="Angle")

        if hlamp.hydra_light_type == 'directional':
            layout.prop(hlamp, "inner_radius", text="Light cone inner radius")
            layout.prop(hlamp, "outer_radius", text="Light cone outer radius")

        layout.prop(hlamp, "color")
        layout.prop(hlamp, "multiplier", text="Multiplier")

        layout.separator()
        layout.label(text="Distribution")
        layout.prop(hlamp, "light_distribution", text="Light distribution", expand=True)

        if hlamp.light_distribution == 'ies':
            layout.prop(hlamp, "ies_path", text="IES")

        if hlamp.light_distribution == 'spot':
            layout.prop(hlamp, "falloff_angle2", text="Inner angle")
            layout.prop(hlamp, "falloff_angle", text="Outer angle")


classes = (
    HydraLamp_PT_lamp,
)


def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class

    for cls in classes:
        unregister_class(cls)
